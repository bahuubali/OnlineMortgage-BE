package com.loanapp.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class CustomerRowMapper implements RowMapper<CustomerDto>{
	
	public CustomerDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		CustomerDto tempCustomer = new CustomerDto();
		tempCustomer.setCustId(rs.getLong("CUS_ID"));
		tempCustomer.setAddress(rs.getString("CUS_ADDR"));
		tempCustomer.setCity(rs.getString("CITY"));
		tempCustomer.setDob(rs.getDate("CUS_DOB"));
		tempCustomer.setEmail(rs.getString("CUS_EMAIL"));
		tempCustomer.setMobile(rs.getString("CUS_TEL"));
		tempCustomer.setName(rs.getString("CUS_NAME"));
		tempCustomer.setPan(rs.getString("CUS_PAN"));
		tempCustomer.setSalary(Integer.toString(rs.getInt("CUS_SAL")));
				return tempCustomer;
	}

}
