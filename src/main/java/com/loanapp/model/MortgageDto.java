package com.loanapp.model;

public class MortgageDto {

	private int mortgageAmount;
	private int mortgageType;
	private int previousLoan;
	private int legalCase;
	private int propertySize;
	
	public int getMortgageAmount() {
		return mortgageAmount;
	}
	public void setMortgageAmount(int mortgageAmount) {
		this.mortgageAmount = mortgageAmount;
	}
	public int getMortgageType() {
		return mortgageType;
	}
	public void setMortgageType(int mortgageType) {
		this.mortgageType = mortgageType;
	}
	public int getPreviousLoan() {
		return previousLoan;
	}
	public void setPreviousLoan(int previousLoan) {
		this.previousLoan = previousLoan;
	}
	public int getLegalCase() {
		return legalCase;
	}
	public void setLegalCase(int legalCase) {
		this.legalCase = legalCase;
	}
	public int getPropertySize() {
		return propertySize;
	}
	public void setPropertySize(int propertySize) {
		this.propertySize = propertySize;
	}
}
