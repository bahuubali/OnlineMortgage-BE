package com.loanapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.loanapp.model.CustomerDto;
import com.loanapp.service.impl.CustomerServiceImpl;

@RestController
@CrossOrigin
public class LoginController {
	
	@Autowired
	private CustomerServiceImpl customerService;
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/login",
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			method = RequestMethod.POST
					)
	public ResponseEntity<CustomerDto> login(@RequestBody CustomerDto customerDto) {
		CustomerDto retCustomer = customerService.checkCustomerExist(customerDto);
		if (retCustomer==null) retCustomer = new CustomerDto ();		
		return new ResponseEntity(retCustomer,HttpStatus.OK);
				
	}
	
}
