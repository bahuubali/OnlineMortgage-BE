package com.loanapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.loanapp.exceptions.MortgageException;
import com.loanapp.model.CustomerDto;
import com.loanapp.model.CustomerRequestDto;
import com.loanapp.service.RequestService;
import com.loanapp.util.KnockoutCheckUtil;

@RestController
@CrossOrigin
public class CustomerController {
	
	@Autowired
	private RequestService requestService;
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/customers",
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			method = RequestMethod.POST
					)
	public ResponseEntity<CustomerRequestDto> createCustomer(@RequestBody CustomerRequestDto customerRequestDet) throws MortgageException {
		System.out.println("Controller()");
		System.out.println("mortgageType :"+customerRequestDet.getMortgageType());
		
		KnockoutCheckUtil knockoutCheckUtil = new KnockoutCheckUtil();

		if(knockoutCheckUtil.mortgageCheck(customerRequestDet.getMortgageType())) {
			throw new MortgageException("You are not eligible due to existing mortgage");
		} 
		
		if(knockoutCheckUtil.legalResidenceCheck(customerRequestDet.getCity())) {
			throw new MortgageException("You are not eligible due to Illegal residence");
		}
		
		/*if(knockoutCheckUtil.creditScoreCheck(customerRequestDet.getPan())) {
			throw new MortgageException("You are not eligible due to low CIBIL score");
		}*/
		
		requestService.create(customerRequestDet);
		
		CustomerRequestDto temp = new CustomerRequestDto();
		temp.setMortgageAmount(customerRequestDet.getMortgageAmount());
		return new ResponseEntity(temp, HttpStatus.OK);		
	}
	
}
