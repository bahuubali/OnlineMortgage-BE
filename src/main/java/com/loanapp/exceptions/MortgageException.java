package com.loanapp.exceptions;

public class MortgageException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8389109538349957485L;
	private String errorMessage;
	
	public MortgageException() {
		super();
	}

	public MortgageException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
}
