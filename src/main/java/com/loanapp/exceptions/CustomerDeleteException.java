package com.loanapp.exceptions;

public class CustomerDeleteException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -117282902085681878L;
	private String errorMessage;
	
	public CustomerDeleteException() {
		super();
	}
	
	public CustomerDeleteException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
}
