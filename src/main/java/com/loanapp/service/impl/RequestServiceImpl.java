package com.loanapp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.loanapp.dao.CustomerDAO;
import com.loanapp.dao.CustomerRequestDAO;
import com.loanapp.dao.MortgageDAO;
import com.loanapp.model.CustomerDto;
import com.loanapp.model.CustomerRequestDto;
import com.loanapp.model.MortgageDto;
import com.loanapp.service.RequestService;

@Service
public class RequestServiceImpl implements RequestService {

	@Autowired
	CustomerDAO customerDAO;
	
	@Autowired
	MortgageDAO mortgageDAO;
	
	@Autowired
	CustomerRequestDAO customerRequestDAO;
	
	
	public int create(CustomerRequestDto requestDet) {		
		
		CustomerDto tempCustomer = new CustomerDto();
		tempCustomer.setAddress(requestDet.getAddress());
		tempCustomer.setCity(requestDet.getCity());
		tempCustomer.setDob(requestDet.getDob());
		tempCustomer.setEmail(requestDet.getEmail());
		tempCustomer.setMobile(requestDet.getMobile());
		tempCustomer.setName(requestDet.getName());
		tempCustomer.setPan(requestDet.getPan());
		tempCustomer.setSalary(requestDet.getSalary());
				int returnStatus = customerDAO.addCustomer(tempCustomer);
		
		if (returnStatus==0) {
			MortgageDto tempMortgage = new MortgageDto();			
			tempMortgage.setLegalCase(requestDet.getLegalCase());
			tempMortgage.setMortgageAmount(requestDet.getMortgageAmount());
			tempMortgage.setMortgageType(requestDet.getMortgageType());
			tempMortgage.setPreviousLoan(requestDet.getPreviousLoan());
			tempMortgage.setPropertySize(requestDet.getPropertySize());
			
			int returnStatusM = mortgageDAO.addMortgage(tempMortgage,tempCustomer.getCustId());
			
			return returnStatusM;
		}
			
			
		return 0;
	}

		

}
