package com.loanapp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.loanapp.dao.CustomerDAO;
import com.loanapp.model.CustomerDto;
import com.loanapp.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	CustomerDAO customerDAO;

	public CustomerDto create(CustomerDto customer) {	
		customerDAO.addCustomer(customer);
		return customer;
	}
	
	public CustomerDto checkCustomerExist(CustomerDto customer) {
		CustomerDto returnObj = null;
		try {
			returnObj = customerDAO.checkCustomerExist(customer);
		}
		catch(Exception e) {
			returnObj = null;
		}
		return returnObj;
	}

}
