package com.loanapp.service;

import com.loanapp.model.CustomerRequestDto;

public interface RequestService {

	int create(CustomerRequestDto requestDet);
}
