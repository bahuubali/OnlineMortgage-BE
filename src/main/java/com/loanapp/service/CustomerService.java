package com.loanapp.service;

import com.loanapp.model.CustomerDto;

public interface CustomerService {

	CustomerDto create(CustomerDto customer);
	
	CustomerDto checkCustomerExist(CustomerDto customer);
}
