package com.loanapp.dao;

import java.util.List;

import com.loanapp.model.CustomerDto;

public interface CustomerDAO {

	int addCustomer(CustomerDto customer);
	
	CustomerDto checkCustomerExist(CustomerDto customer) throws Exception;
}
