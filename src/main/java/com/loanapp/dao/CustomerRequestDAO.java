package com.loanapp.dao;

import com.loanapp.model.CustomerDto;

public interface CustomerRequestDAO {

	int addCustomerRequest(CustomerDto customer);
}
