package com.loanapp.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class KnockoutValidationDao {


	@Autowired
    JdbcTemplate jdbcTemplate;

	public int checkCreditScore(String pan) {
		Integer creditScore = jdbcTemplate.queryForObject("SELECT cibil_score from credit_score where pan_id= ?",new Object[] { pan.toLowerCase() }, Integer.class);
		
		return creditScore;
	}
}
