package com.loanapp.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.loanapp.dao.CustomerDAO;
import com.loanapp.model.CustomerDto;
import com.loanapp.model.CustomerRowMapper;

@Component
public class CustomerDAOImpl implements CustomerDAO{

	@Autowired
    JdbcTemplate jdbcTemplate;

	public int addCustomer(CustomerDto customer) {
		jdbcTemplate.update("INSERT INTO customer (CUS_NAME,CUS_DOB,CUS_ADDR,CUS_EMAIL,CUS_TEL,CUS_PAN,CUS_SAL,CITY) VALUES (?, ?, ?, ?,?,?,?,?)",
		           customer.getName(),
		           customer.getDob(),
		           customer.getAddress(),
		           customer.getEmail(),
		           customer.getMobile(),
		           customer.getPan(),
		           customer.getSalary(),
		           customer.getCity());
		
		
		Long max_cusID= jdbcTemplate.queryForObject("SELECT CUS_ID FROM customer WHERE CUS_ID=(SELECT max(CUS_ID) FROM customer)",Long.class);

		customer.setCustId(max_cusID);
			return 0;		
	}

	public CustomerDto checkCustomerExist(CustomerDto customerInp) throws Exception {
		
		System.out.println(customerInp.getCustId());
		String sql = "SELECT * FROM CUSTOMER WHERE CUS_EMAIL = ?";

		return jdbcTemplate.queryForObject(
				sql, new Object[] { customerInp.getEmail() }, new CustomerRowMapper());			
		
	}
	
	
}
