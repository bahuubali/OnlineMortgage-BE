package com.loanapp.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.loanapp.dao.MortgageDAO;
import com.loanapp.model.MortgageDto;

@Component
public class MortgageDAOImpl implements MortgageDAO{

	@Autowired
    JdbcTemplate jdbcTemplate;

	public int addMortgage(MortgageDto mortgage,Long cusId) {
		jdbcTemplate.update("INSERT INTO mortgage (customer_id,mortgage_type,mortgage_amt,existing_loan,legal_cases,property_size) VALUES (?, ?, ?, ?,?,?)",
				cusId,
				mortgage.getMortgageType(),
				mortgage.getMortgageAmount(),
				mortgage.getPreviousLoan(),
				mortgage.getLegalCase(),
				mortgage.getPropertySize()
				
		           );
		
		
		
			return 0;		
	}
}
