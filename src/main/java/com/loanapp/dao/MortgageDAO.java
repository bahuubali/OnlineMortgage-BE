package com.loanapp.dao;

import com.loanapp.model.MortgageDto;

public interface MortgageDAO {

	int addMortgage(MortgageDto mortgage,Long cusId);
}
