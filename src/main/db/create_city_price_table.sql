CREATE TABLE `mortgagedb`.`city_price` (
  `city_id` INT NOT NULL,
  `city_name` VARCHAR(45) NOT NULL,
  `city_price` INT NULL,
  PRIMARY KEY (`city_id`));