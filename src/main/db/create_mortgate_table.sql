CREATE TABLE `mortgagedb`.`mortgage` (
  `customer_id` INT NOT NULL,
  `mortgage_type` VARCHAR(45) NULL,
  `mortgage_amt` INT NULL,
  `existing_loan` INT NULL COMMENT '1 for existing loan and 0 for no loan',
  `legal_cases` INT NULL COMMENT '1 for leaglcase and 0 for no case',
  INDEX `FK_customer_id_idx` (`customer_id` ASC),
  CONSTRAINT `FK_customer_id`
    FOREIGN KEY (`customer_id`)
    REFERENCES `mortgagedb`.`customer` (`CUS_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
COMMENT = 'Mortage details for a given customer.';

ALTER TABLE `mortgagedb`.`mortgage` 
ADD COLUMN `property_size` INT NULL AFTER `legal_cases`;