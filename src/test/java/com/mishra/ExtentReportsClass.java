package com.mishra;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
 
public class ExtentReportsClass
{
	ExtentReports extent;
	ExtentTest logger;
	WebDriver driver;
	String driverPath = "./src/test/Drivers/";
	
	public WebElement expandRootElement(WebElement element) 
	{
	
	WebElement ele = (WebElement)((JavascriptExecutor)driver).executeScript("return arguments[0].shadowRoot", element);
	
	return ele;
	
	}


@BeforeTest
public void startReport(){
	
	extent = new ExtentReports (System.getProperty("user.dir") +"/test-output/STMExtentReport.html", true);
			extent
            .addSystemInfo("Host Name", "Hackathon")
            .addSystemInfo("Environment", "Automation Testing")
            .addSystemInfo("User Name", "Girish Satyam");
            extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
}

@BeforeTest
public void setUp() 
{

	System.out.println("Opening chrome browser");
	System.setProperty("webdriver.chrome.driver", driverPath + "chromedriver.exe");
	driver = new ChromeDriver();

}	

@Test
public void testGetText_FromShadowDOMElements()
{
	System.out.println("Open Chrome downloads");
	driver.manage().deleteAllCookies();
	driver.manage().window().maximize();
	driver.navigate().to("http://127.0.0.1:8081/");  
	
	System.out.println("Validate downloads page header text");
	/////////////////
	

WebElement root1 = driver.findElement(By.tagName("ma-app"));
WebElement shadowRoot1 = expandRootElement(root1);

//WebElement root2 = shadowRoot1.findElement(By.tagName("app-drawer-layout"));
//WebElement shadowRoot2 = expandRootElement(root2);

WebElement root3 = shadowRoot1.findElement(By.cssSelector("app-header-layout"));
WebElement shadowRoot3 = expandRootElement(root3);

//iron-pages

WebElement root4 = shadowRoot3.findElement(By.cssSelector("iron-pages"));
WebElement shadowRoot4 = expandRootElement(root3);


WebElement root5 = shadowRoot4.findElement(By.cssSelector("ma-new-customer"));
WebElement shadowRoot5 = expandRootElement(root3);


shadowRoot5.findElement(By.id("name")).sendKeys("First Name");




////////////////////////////
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
/*	
	
	WebElement root1 = driver.findElement(By.tagName("ma-app"));
	WebElement shadowRoot1 = expandRootElement(root1);
	 //shadowRoot1 = expandRootElement(root1);
	
	WebElement root2 = shadowRoot1.findElement(By.cssSelector("app-drawer-layout"));
	
	
	//Get shadow root element
	
	//WebElement shadowRoot2 = expandRootElement(root2);
	
	
	// Enter the Value of  Name
	//shadowRoot1.findElement(By.cssSelector("paper-input")).sendKeys("First Name");
*/	
	
	/*//Pass the Value in the DOB Field
	shadowRoot1.findElement(By.id("dob")).sendKeys("10/10/1990");
	
	//Pass the Value in Email Field 
	shadowRoot1.findElement(By.id("emailAddress")).sendKeys("girish.satyam@yahoo.com");
	
	//Pass the Value in Mobile Number Field 
	shadowRoot1.findElement(By.id("mobile")).sendKeys("9876543234");
	
	
	//Pass the Value in Social Security Number Field 
	shadowRoot1.findElement(By.id("pan")).sendKeys("879897898797");
	
	//Pass the Value in Salary Field 
	shadowRoot1.findElement(By.id("salary")).sendKeys("78978978787");
	
	
	//Pass the Value in PinCode Field 
	shadowRoot1.findElement(By.id("pincode")).sendKeys("9876543");	
	
	
	//Pass the Value in CITY Field 
	shadowRoot1.findElement(By.id("city")).sendKeys("Bangalore");
	
	//Click on the Submit Button
	shadowRoot1.findElement(By.id("submitButton")).click();
	
	
	
	
	System.out.println("Form Submission is done Succesfully");
	
	

*/
}
}