package com.mishra;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class TestApi_2 

{
	ExtentReports extent;
	ExtentTest logger;
	WebDriver driver;
	String driverPath = "./src/test/Drivers/";
	/*
	 * Get the Response in JSON Format
	 * When I give the Response as Get
	 * Then the Status Code is 200 OK
	 */
	
	
	@BeforeTest
	public void startReport(){
		
		extent = new ExtentReports (System.getProperty("user.dir") +"/test-output/STMExtentReport.html", true);
				extent
	            .addSystemInfo("Host Name", "Hackathon")
	            .addSystemInfo("Environment", "Automation Testing")
	            .addSystemInfo("User Name", "Girish Satyam");
	            extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
	}

/*	@BeforeTest
	public void setUp() 
	{

		System.out.println("Opening chrome browser");
		System.setProperty("webdriver.chrome.driver", driverPath + "chromedriver.exe");
		driver = new ChromeDriver();

	}*/		
	@Test
	public void TestAddUser()
	{
		String  userCredential ="{\"custId\":\"10\",\"name\":\"john\",\"email\":\"john@email.com\",\"mobile\":\"8978798\",\"city\":\"bangalore\",\"address\":\"jayanagar\",\"salary\":\"500000\", \"pan\":\"8798789789\",\"mortgageAmount\":\"1000\",\"mortgageType\":\"0\",\"previousLoan\":\"0\",\"legalCase\":\"0\"}";
    	RestAssured.baseURI  = "http://10.117.189.188:8080/OnlineMortgage/customers";	
    	Response r = RestAssured.given()
    				.contentType("application/json")
    				.body(userCredential)
    				.when().post("");
      
    	
    	//validate status code
    	//Assert.assertEquals(200, expected);
    	System.out.println("Status Code is -->  "+r.getStatusCode());
    	Assert.assertEquals(200, r.getStatusCode()); 
    	logger = extent.startTest("passTest");
		Assert.assertTrue(true);
		//To generate the log when the test case is passed
		logger.log(LogStatus.PASS, "Test Case Passed is passTest");
		
}
	
	@AfterMethod
	public void getResult(ITestResult result){
		if(result.getStatus() == ITestResult.FAILURE){
			logger.log(LogStatus.FAIL, "Test Case Failed is "+result.getName());
			logger.log(LogStatus.FAIL, "Test Case Failed is "+result.getThrowable());
		}else if(result.getStatus() == ITestResult.SKIP){
			logger.log(LogStatus.SKIP, "Test Case Skipped is "+result.getName());
		}
		// ending test
		//endTest(logger) : It ends the current test and prepares to create HTML report
		extent.endTest(logger);
	}
	@AfterTest
	public void endReport(){
		// writing everything to document
		//flush() - to write or update test information to your report. 
                extent.flush();
                extent.close();
    }
}